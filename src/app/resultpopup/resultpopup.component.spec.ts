import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultpopupComponent } from './resultpopup.component';

describe('ResultpopupComponent', () => {
  let component: ResultpopupComponent;
  let fixture: ComponentFixture<ResultpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
