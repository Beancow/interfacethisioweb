import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemopageComponent } from './demopage/demopage.component';

const routes: Routes = [
  { path: '', component: DemopageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
