import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpLink } from 'apollo-angular-link-http';
import { Apollo } from 'apollo-angular';
import { InMemoryCache } from 'apollo-cache-inmemory';
import gql from 'graphql-tag';

import { AIResultType } from '../models/ai-result-type';
import { DialogData } from '../models/DialogData';
import { ResultpopupComponent } from '../resultpopup/resultpopup.component';

@Component({
  selector: 'app-demopage',
  templateUrl: './demopage.component.html',
  styleUrls: ['./demopage.component.css']
})
export class DemopageComponent implements AfterViewInit {

  public airesults: AIResultType[];

  public returnedresults;

  public resultsbydestination: DialogData;
 
  InterfaceThisdemoForm: FormGroup;
  InterfaceThisReturnedDemoForm: FormGroup;

  fileName: string = 'none';

  constructor(public dialog: MatDialog, private apollo: Apollo, httpLink: HttpLink, private cd: ChangeDetectorRef) {  

    apollo.create({
      link: httpLink.create({ uri: 'https://us-central1-interfacethisio.cloudfunctions.net/api/graphql' }),
      cache: new InMemoryCache()
    })

    this.InterfaceThisReturnedDemoForm = new FormGroup({
      subscriptions: new FormControl(),
    });
    this.InterfaceThisReturnedDemoForm.controls.subscriptions.setValue('');

    
    this.InterfaceThisdemoForm = new FormGroup({
      subscriptions: new FormControl(),
      firstname: new FormControl(),
      surname: new FormControl(),
      testresone: new FormControl(),
      testrestwo: new FormControl(),
      image: new FormControl({filename: null, filedata: null}, [Validators.required])
    });
    this.InterfaceThisdemoForm.controls.subscriptions.setValue('');
    this.InterfaceThisdemoForm.controls.firstname.setValue('');
    this.InterfaceThisdemoForm.controls.surname.setValue('');
    this.InterfaceThisdemoForm.controls.testresone.setValue('');
    this.InterfaceThisdemoForm.controls.testrestwo.setValue('');

  }

  ngAfterViewInit(): void {
    this.getData()
  }

  openDialog(): void {
    console.log(this.resultsbydestination)
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = this.resultsbydestination;
    let dialogRef = this.dialog.open(ResultpopupComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  public addData = (vars) => {

    if (vars.image.imagename != null) {
      
      let userVar = {
        "PatientRecord": vars
      }

      this.apollo.mutate({
        mutation: gql`mutation($PatientRecord: PatientData){
          addData(PatientRecord: $PatientRecord){
            firstname
            surname
            testresone
            testrestwo
            image {
              imagename
              imagedata
            }
          }
        }`,
        variables: userVar }).subscribe(result => {
        this.airesults = result.data as AIResultType[];
      })
    }
  }
  
  public getData = (token?: string) => {

    if (!token) {
      this.apollo.query({
        query: gql`query{
          patients {
            docid
            patientrecord {
              firstname
              surname
              testresone
              testrestwo
              subscriptions
              image {
                imagename
              }
            }
          }
        }`}).subscribe(result => {
         this.returnedresults = result.data['patients'] as AIResultType[]
      })
    }
    if (token) {
      let varsToken = {
        token: token
      }

      console.table(varsToken)
      
      this.apollo.query({
        query: gql`query($token: String){
          aisubscriptions(token: $token){
            docid
            patientrecord {
              firstname
              surname
              testresone
              testrestwo
              subscriptions
              image {
                imagename
              }
            }
          }
        }`,
         variables: varsToken }).subscribe(result => {
         this.returnedresults = result.data['aisubscriptions'] as AIResultType[]
      })
      this.cd.markForCheck();
    }
    
  }

  loadResultsBySub(subscription, docid) {
    let varsToken = {
      document: {
      token: subscription,
      docid: docid
      }
    }

    this.apollo.query({
      query: gql`query($document: ResultInput) {
        airesults(document: $document){
          testresone
          testrestwo
          image {
            imagename
            imagedata
          }
        }
      }`,
      variables: varsToken }).subscribe(result => {
         if (result.data['airesults'][0].testresone !== null) {
          this.resultsbydestination = result.data['airesults'][0] as DialogData;
          this.openDialog(); 
      }
    })
  }


  onSuccess(res) {
    console.log(res)
  }

  onErr(err) {
    console.table(err);
  }

  onFileChange(event) {
    let reader = new FileReader();
   
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.InterfaceThisdemoForm.patchValue({
          image: {
            imagename: file.name,
            imagedata: reader.result
          }
        });

        this.fileName = file.name;
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }       
}
