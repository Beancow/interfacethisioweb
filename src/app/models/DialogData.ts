export interface DialogData {
    testresone: string;
    testrestwo: string;
    image: {
      imagename: string;
      imagedata: string;
    }
}