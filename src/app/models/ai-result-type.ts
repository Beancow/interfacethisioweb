export type AIResultType = {
    list: {
        [patients: string]: {
            docid: string
            patientrecord: {
            firstname: string
            surname: string
            testresone: string
            testrestwo: string
            subscriptions: [string]
            image: {
                imagename: string
                imagedata: string
                }
            }
        }
    }
}