import { Component, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogData } from '../models/DialogData';

@Component({
  selector: 'app-resultpopup',
  templateUrl: './resultpopup.component.html',
  styleUrls: ['./resultpopup.component.css']
})
export class ResultpopupComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
}
